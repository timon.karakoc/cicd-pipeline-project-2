variable "location" {
  default = "germanywestcentral"
}

variable "tags" {
	default = {
	CreatedBy   = "TimonKarakoc"
    neededUntil = "2021-11-01"
    Environment = "Terraform Getting Started"
    Team        = "DevOps"
	}
}