terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }
  required_version = ">= 0.14.9"
  backend "azurerm" {
    resource_group_name  = "rg-karakoc-terraform-backend"
    storage_account_name = "karakocterraformbackend"
    container_name       = "vhds"
    key                  = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-karakoc-terraform-deploy"
  location = var.location
  tags = var.tags
}

resource "azurerm_container_registry" "acr" {
  name                = "crkarakoctf"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "standard"
  admin_enabled       = false
}