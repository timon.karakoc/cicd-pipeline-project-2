variable "resource_group_name" {
 	default = "rg-karakoc-terraform"
}
variable "location" {
	default = "westeurope"
}
variable "creator" {
	default = "TimonKarakoc"
}
variable "neededUntil" {
	default = "2021-10-08"
}