output "resource_group_id" {
  value = azurerm_resource_group.rg.id
}
output "CreatedBy"{
	value = var.creator
}
output "neededUntil"{
	value = var.neededUntil
}