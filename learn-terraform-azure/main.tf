terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }
  required_version = ">= 0.14.9"
}
provider "azurerm" {
  features {}
}

locals {
  default_tags = {
    "createdBy"   = var.createdBy
    "neededUntil" = var.neededUntil
    "project"     = var.project
  }
}


resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.location
  tags = {
    CreatedBy = var.creator
    neededUntil = var.neededUntil
    Environment = "Terraform Getting Started"
    Team = "DevOps"
  }
}

resource "azurerm_container_registry" "acr" {
  name                = "crkarakoctf"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = "standard"
  admin_enabled       = false
}

